## Program to record single words in order to collect a training dataset for TensorFlow. [WIP]


#### Required packages (installation info for Linux-based systems):
* auditok (main package that splits audio into pieces, e.g. tokenizes it)  
  install via: `sudo pip install auditok`
* PyAudio (used to play back the recording)  
  install via: `sudo apt-get install python-pyaudio` (pip installation seems to fail, see [StackOverflow](https://stackoverflow.com/questions/5921947/pyaudio-installation-error-command-gcc-failed-with-exit-status-1) for further info)  
* matplotlib (used to display the graph)  
  install via: `sudo pip install matplotlib`
* NumPy (to deal with crunching numbers for matplotlib)  
  install via: `sudo apt-get install python-numpy `  

  Edited in [eric IDE](https://eric-ide.python-projects.org/).
  
#### Usage:
Set your desired words to record in the file `words.json`.  
Then run `main.py` in a terminal via `python main.py` and follow the instructions on-screen.  
Enter your name and a descriptive word of the situation you are in. This could be "background_music" or "fan_noise" or something similar.  
Each word is recorded and then played back and shown on-screen as a waveform. If it seems to be correct, press 'Y' and/or 'ENTER' to continue.
An incorrect word will have to be recorded again.  
The files are saved in the folder `audio_files/` in folders corresponding to the recorded word.
They are named after your name and situation and end in "nohash_0.wav" or the corresponding number.  
This is so that TensorFlow doesn't train on the same situation and get falsely good results.  
See [the TensorFlow speech recognition tutorial](https://www.tensorflow.org/tutorials/audio_recognition#custom_training_data) for further information.

Code basis taken from the [Auditok API tutorial](http://auditok.readthedocs.io/en/latest/apitutorial.html).
