import sys
from auditok import ADSFactory, AudioEnergyValidator, StreamTokenizer, player_for
import wave
import json
import helper_funcs as helper
import plotting

sampling_rate = 16000  # Number of samples/sec
max_time = 2  # Maximum recording time in seconds
sample_width = 2  # Number of bytes per sample (2 e.g. 16 bit for TensorFlow)

if len(sys.argv) <= 1:
    settings = json.load(open("words.json"))  # Load file "words.json" into a dictionary called "settings"
    print("To specify words that should be recorded, add the filename as an argument. " +
          "Using words.json per default. \n\n\n\n")
else:
    settings = json.load(open(sys.argv[1]))  # Load file specified as an argument

asource = ADSFactory.ads(record=True, max_time=max_time, sampling_rate=sampling_rate)
validator = AudioEnergyValidator(sample_width=asource.get_sample_width(), energy_threshold=65)
tokenizer = StreamTokenizer(validator=validator, min_length=50, max_length=1000, max_continuous_silence=30)

player = player_for(asource)

helper.create_dir("audio_files")  # Create folder to store the audio files if it doesn't exist

print("###############################################################")
print("#       Script to record single words from a microphone.      #")
print("#   Change the values in the file 'words.json' to set words.  #")
print("#    For further usage information, please view 'README.md'   #")
print("###############################################################\n\n")
name = raw_input("Please enter your name: ")
situation = raw_input("Please enter something that describes your current situation. \n" +
                      "A situation could be background noise, or another microphone for example: ")
for i in xrange(settings["number_of_recordings_per_word"]):
    for word_item in settings["words"]:
        successful = False
        while not successful:
            print("\n\n\nPlease say \"%s\" " % word_item["word"])
            recorded_something = False
            while not recorded_something:
                asource = ADSFactory.ads(record=True, max_time=2, sampling_rate=16000)
                asource.open()  # open the audio stream
                recording = tokenizer.tokenize(asource)  # tokenize it (e.g. split it up into single words)
                asource.close()
                if len(recording) > 0:  # if it recorded anything
                    data = b''.join(recording[0][0])  # get the first token
                    plotting.plot(data, sample_width,
                                  sampling_rate)  # plot the data for a graphic representation of its validity
                    player.play(data)  # play back the gathered token
                    recorded_something = True
            if (helper.query_yes_no("Is the recording complete and without error?",
                                    default="yes")):  # Wait until console input
                filename = "audio_files/%s/%s_%s_nohash_%d.wav" % (word_item["label"], name, situation, i)
                if i == 0:
                    helper.create_dir("audio_files/%s" % word_item["label"])
                print("Saving to %s" % filename)
                file = wave.open(filename, "w")
                file.setnchannels(1)  # Mono wave file (only one channel)
                file.setframerate(sampling_rate)
                file.setsampwidth(sample_width)
                file.setnframes(
                    1)  # Only one audio sample to begin with, number gets automatically enlarged by writeframes
                file.writeframes(b''.join(recording[0][0]))
                successful = True
