from auditok import AudioEnergyValidator
import numpy as np
import matplotlib.pyplot as plt
"""
Plotting function adapted from auditok.cmdline functions _plot and plot_all
"""


def plot(data, sample_width,  sampling_rate):
    plt.clf()  # Clear the figure on the plot
    plt.ioff()  # Turn off interactive mode, so as to not lose focus on spawning the window
    # convert audio data to energy levels, e.g. able to be shown as a waveform
    signal = AudioEnergyValidator._convert(data, sample_width)
    signal /= np.max(np.abs(signal), axis=0)  # normalize it to -1;1, source: https://stackoverflow.com/a/
    # generate x-Axis data from the number of samples and the sampling_rate
    t = np.arange(0., np.ceil(float(len(signal))) / sampling_rate, 1./sampling_rate)
    if len(t) > len(signal):
        t = t[: len(signal) - len(t)]  # Make sure the x-Axis isn't longer than the amount of data supplied
    plt.plot(t, signal)  # Plot the graph
    plt.xlabel("Time (s)", fontsize=12)  # Create Axis labels
    plt.ylabel("Amplitude (normalized)", fontsize=12)
    plt.show(block=False)  # Show it in a non-blocking manner so that the rest of the program can continue running
